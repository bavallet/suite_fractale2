#include<iostream>
#include<vector>

std::vector<double> suiteFractale2(int n){

    std::vector<double> vec = std::vector<double>();

    double val = 0.5;
    double valAdd;
    double currVal;

    if(n >=0) vec.push_back(val);

    for(int i=0; i<n ;++i)
    {
        valAdd = val;
        val *=0.5;
        currVal = val;

        while(currVal < 1){
            vec.push_back(currVal);
            currVal += valAdd;
        }
    }
    return vec;
}

void AfficherFractale(std::vector<double> vec){
    std::cout<<std::endl<<"Fractale : ";

    for(auto d : vec ) std::cout<< std::to_string(d)<<" ; ";
    std::cout<<std::endl;
}


int main(){
    int dimension = 0;
    std::vector<double> vec = std::vector<double>();

    while(dimension>=0){
        std::cout<<"Indiquez la dimension, négatif pour arrêter"<<std::endl;
        std::cin>> dimension;
        if (dimension>=0){
            vec = suiteFractale2(dimension);
            AfficherFractale(vec);
            vec.clear();
        }
    }
    return 0;
}