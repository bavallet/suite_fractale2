SRC = suiteFractale.cpp
OBJ = $(subst .cpp,.o,$(SRC))
RM=rm -f

suiteFractale: $(OBJ)
	g++ -o suiteFractale $(OBJ)

suiteFractale.o: suiteFractale.cpp
	g++ -c suiteFractale.cpp